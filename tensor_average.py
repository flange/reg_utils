#!/usr/bin/env python3
#
# Date: 21/09/2022
# Author: Frederik J Lange
# Copyright: FMRIB 2022

import numpy as np
from fsl.data.image import Image
import argparse

# Performs a fast matrix logarithm using Eigen decomposition. NB! Assumes matrix is SPD and does not check if matrix is zero.
def fast_logm(A):
    A_mask = ~np.isfinite(A)
    A[A_mask] = 0
    W,V = np.linalg.eigh(A)
    log_A = np.matmul(V * np.log(W)[..., None, :], np.linalg.inv(V))
    log_A[~np.isfinite(log_A)] = np.nan
    log_A[A_mask] = np.nan
    return log_A

# Performs a fast matrix exponention using Eigen decomposition. NB! Assumes matrix is symmetric.
def fast_expm(A):
    A_mask = ~np.isfinite(A)
    A[A_mask] = 0
    W,V = np.linalg.eigh(A)
    exp_A = np.matmul(V * np.exp(W)[..., None, :], np.linalg.inv(V))
    exp_A[~np.isfinite(exp_A)] = np.nan
    exp_A[A_mask] = np.nan
    return exp_A

# Convert linearised upper-diagonal 3x3 matrix to full 3x3 matrix
def tensor_fsl_to_full(tensor_fsl):
    tensor_full = np.zeros(tensor_fsl.shape[0:3] + (3,3))
    tensor_full[..., 0, 0] = tensor_fsl[..., 0]
    tensor_full[..., 0, 1] = tensor_full[..., 1, 0] = tensor_fsl[..., 1]
    tensor_full[..., 0, 2] = tensor_full[..., 2, 0] = tensor_fsl[..., 2]
    tensor_full[..., 1, 1] = tensor_fsl[..., 3]
    tensor_full[..., 1, 2] = tensor_full[..., 2, 1] = tensor_fsl[..., 4]
    tensor_full[..., 2, 2] = tensor_full[..., 2, 2] = tensor_fsl[..., 5]
    return tensor_full

# Convert full 3x3 matrix to linearised upper-diagonal matrix
def tensor_full_to_fsl(tensor_full):
    tensor_fsl = np.zeros(tensor_full.shape[0:3] + (6,))
    tensor_fsl[..., 0] = tensor_full[..., 0, 0] 
    tensor_fsl[..., 1] = tensor_full[..., 0, 1] 
    tensor_fsl[..., 2] = tensor_full[..., 0, 2] 
    tensor_fsl[..., 3] = tensor_full[..., 1, 1] 
    tensor_fsl[..., 4] = tensor_full[..., 1, 2] 
    tensor_fsl[..., 5] = tensor_full[..., 2, 2] 
    return tensor_fsl

# Calculate the average log-tensor. NB! All tensors must have the same 3D dimenstions.
def log_tensor_average(tensor_paths):
    avg_tensor_img = Image(tensor_paths[0])
    avg_tensor_data = np.zeros(avg_tensor_img.shape[0:3] + (3,3))
    valid_tensor_data = np.zeros(avg_tensor_img.shape[0:3] + (3,3))

    for i, tensor_path in enumerate(tensor_paths, start = 1):
        print('Processing tensor', i, 'of', len(tensor_paths))
        tensor_img = Image(tensor_path)
        tensor_data = tensor_fsl_to_full(1e6 * tensor_img[:])
        log_tensor_data = fast_logm(tensor_data)
        log_tensor_nans = np.isnan(log_tensor_data)
        avg_tensor_data[~log_tensor_nans] = avg_tensor_data[~log_tensor_nans] + log_tensor_data[~log_tensor_nans]
        valid_tensor_data = valid_tensor_data + (~log_tensor_nans).astype(int)
    
    avg_tensor_data[valid_tensor_data > 0] = avg_tensor_data[valid_tensor_data > 0]/valid_tensor_data[valid_tensor_data > 0]
    avg_tensor_data = fast_expm(avg_tensor_data)
    avg_tensor_data[valid_tensor_data <= 0] = 0
    avg_tensor_data = tensor_full_to_fsl(1e-6 * avg_tensor_data)
    avg_tensor_img[:] = avg_tensor_data
    return avg_tensor_img

# Calculate the weighted average log-tensor. NB! All tensors and weights must have the same 3D dimenstions.
def log_tensor_average_weighted(tensor_paths, weight_paths):
    avg_tensor_img = Image(tensor_paths[0])
    avg_tensor_data = np.zeros(avg_tensor_img.shape[0:3] + (3,3))
    valid_tensor_data = np.zeros(avg_tensor_img.shape[0:3] + (3,3))

    for i, (tensor_path, weight_path) in enumerate(zip(tensor_paths, weight_paths), start=1):
        print('Processing tensor', i, 'of', len(tensor_paths))
        tensor_img = Image(tensor_path)
        tensor_data = tensor_fsl_to_full(1e6 * tensor_img[:])
        log_tensor_data = fast_logm(tensor_data)
        log_tensor_nans = np.isnan(log_tensor_data)
        weight_img = Image(weight_path)
        weight_img_data = weight_img[:]
        weight_img_data = np.repeat(weight_img_data[:, :, :, np.newaxis], 3, axis = 3)
        weight_img_data = np.repeat(weight_img_data[:, :, :, :, np.newaxis], 3, axis = 4)
        avg_tensor_data[~log_tensor_nans] = avg_tensor_data[~log_tensor_nans] + log_tensor_data[~log_tensor_nans] * weight_img_data[~log_tensor_nans]
        valid_tensor_data = valid_tensor_data + (~log_tensor_nans).astype(int) * weight_img_data
    
    avg_tensor_data[valid_tensor_data > 0] = avg_tensor_data[valid_tensor_data > 0]/valid_tensor_data[valid_tensor_data > 0]
    avg_tensor_data = fast_expm(avg_tensor_data)
    avg_tensor_data[valid_tensor_data <= 0] = 0
    avg_tensor_data = tensor_full_to_fsl(1e-6 * avg_tensor_data)
    avg_tensor_img[:] = avg_tensor_data
    return avg_tensor_img

flags = {
    'tensor_list'   : ('-t', '--tensor_list'),
    'weight_list'   : ('-w', '--weight_list'),
    'output'        : ('-o', '--output')
}
helps = {
    'tensor_list'   : 'Text file where each row is the path to an FSL format 4D tensor file',
    'weight_list'   : 'Text file where each row is the path to a 3D weighting file',
    'output'        : 'Basename of output FSL format 4D tensor file'
}

parser = argparse.ArgumentParser(description='Calculate the (optionally weighted) logarithmic average of a list of tensor images. NB: The first 3 dimensions of ALL files must match!')
parser.add_argument(*flags['tensor_list'],
                    help=helps['tensor_list'],
                    required=True)
parser.add_argument(*flags['weight_list'],
                    help=helps['weight_list'],
                    required=False)
parser.add_argument(*flags['output'],
                    help=helps['output'],
                    required=True)
args = parser.parse_args()

with open(args.tensor_list, 'r') as f:
    tensor_paths = f.read().splitlines()

if args.weight_list is None:
    avg_tensor_img = log_tensor_average(tensor_paths)
else:
    with open(args.weight_list, 'r') as f:
        weight_paths = f.read().splitlines()
    avg_tensor_img = log_tensor_average_weighted(tensor_paths, weight_paths)

avg_tensor_img.save(args.output)