# reg_utils

A repository of useful registration utilities.

## tensor_average

Calculates the (optionally weighted) average of a list of FSL DTI images.
Useful when creating templates.
NB: All tensor and weight images must have the same dimensions.